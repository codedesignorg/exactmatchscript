/*
	Description: Exact Match Script, only for Alpha Campaigns, on the MCC level. The script interates through all the accounts eneabled in the 'Dashboard' (which can be found on Google Drive),
				 selects all the 'Exact match(close variation)' search terms to the Negative Keywords list, on the Adgroup level.
    Date: 30/07/2018
    Author: Andrei Ciobanu
*/


function main() {
 
  	// Declare the spreadsheet variables.
    var SPREADSHEET_ID_IMPORT ='1ILo7uiRKU23YahYfjooLqnrrtALjvffJkWpivxjsDIg'; //Trial8
  	var SPREADSHEET_URL_IMPORT ='https://drive.google.com/open?id=1ILo7uiRKU23YahYfjooLqnrrtALjvffJkWpivxjsDIg';
	var SHEET_NAME_IMPORT = 'Dashboard';
	var ss_IMPORT = SpreadsheetApp.openById(SPREADSHEET_ID_IMPORT);
	var sheet_IMPORT = ss_IMPORT.getSheetByName(SHEET_NAME_IMPORT);
	
    // Declare the storage arrays.
  	var lastRow = sheet_IMPORT.getLastRow();
    var storage_ID=[], storage_Name=[], storage_Enable=[],storage_Time=[];
    var index=0;
  
  	Logger.log("Start!");
  	Logger.log("Go through the dashboard: ");
  
  	// Iterate through the 'Dashboard' and store all the information
	while (lastRow!=1 )
			{
				var lastColumn = sheet_IMPORT.getLastColumn();
				var lastCell = sheet_IMPORT.getRange(lastRow,lastColumn);
          
          		//Logger.log('Current cell is at %s and has value: "%s".',index,lastCell.getValue());	
          			storage_Time[index]=lastCell.getValue(); lastColumn--;lastCell = sheet_IMPORT.getRange(lastRow,lastColumn);
          		//Logger.log('Current cell is at %s and has value: "%s".',index,lastCell.getValue());
          			storage_Enable[index]=lastCell.getValue().toString(); lastColumn--;lastCell = sheet_IMPORT.getRange(lastRow,lastColumn);
          		//Logger.log('Current cell is at %s and has value: "%s".',index,lastCell.getValue());
          			storage_ID[index]=lastCell.getValue().toString(); lastColumn--;lastCell = sheet_IMPORT.getRange(lastRow,lastColumn);
          		Logger.log('Current cell is at %s and has value: "%s".',index,lastCell.getValue());
          			storage_Name[index]=lastCell.getValue().toString(); lastColumn--;
          	
				lastColumn=sheet_IMPORT.getLastColumn();
				lastRow--;
      			index++;
			}  
  
  	//Logger.log(storage_Enable);
  
	//======================================================================================================================
  	// SELECT ONLY THE INFORMATION THAT WE NEED (Index,Time,Enable,ID,Name)	
  
  	// Declare the arrays for the valid accounts and associated information (valid = enabled in the 'Dashboard')
	var Valid_indexes = [],Valid_storage_Time = [],Valid_storage_Enable = [],Valid_storage_ID = [],Valid_storage_Name = [];
    var local = 0;
  	Logger.log("\n");Logger.log("Selected accounts:");
  
  	for (var i=0;i<=index-1;i++)
  		{
  			if (storage_Enable[i] === "true")
        		{
                	Valid_indexes.push(i); Valid_storage_Time.push(storage_Time[i]);Valid_storage_Enable.push(storage_Enable[i]);Valid_storage_ID.push(storage_ID[i]);Valid_storage_Name.push(storage_Name[i]);
                	Logger.log("Index: %s, time: %s, enable: %s, Id: %s, name: %s",Valid_indexes[local],Valid_storage_Time[local],Valid_storage_Enable[local],Valid_storage_ID[local],Valid_storage_Name[local]);
              		local++;
        		}
  		}
      
  	Logger.log("\n");
	//==========================================================================================================
  	// ITERATE THROUGH THE ACCOUNTS AND ASSOCIATED CAMPAIGNS
  
  	index = Valid_storage_ID.length - 1;
  
  	Logger.log('STEP 1: Completed'); // For debugging purposes
  
  	// Declare variables to interate through the accounts.
  	var accountIterator = MccApp.accounts().withIds(Valid_storage_ID).get(); // Only the selected accounts (validated).
	var accountCounter = 0;
	var mccAccount = AdWordsApp.currentAccount();  // Save MCC account, to switch back later.
    SpreadsheetApp.setActiveSpreadsheet(ss_IMPORT);
    ss_IMPORT.setActiveSheet(sheet_IMPORT);
  
  	// Iterate though the accounts
  	while (accountIterator.hasNext())
  			{
              
    		Logger.log('Has Next');
    		var account = accountIterator.next();
    		MccApp.select(account);
      
            // Retrieve all alpha campaigns.
			var campaignIterator = AdWordsApp.campaigns()
							 			 	 .withCondition("Name CONTAINS_IGNORE_CASE 'alpha'")
							 			 	 .withCondition("Name CONTAINS_IGNORE_CASE 'search'")
							 			 	 .withCondition("Status = ENABLED")
            							 	 .get();
			
            // Iterate through the selected campaigns		
     		//while (campaignIterator.hasNext())
					//{		
						var campaign = campaignIterator.next();
						Logger.log('Current account ID: %s and Name: %s', account.getCustomerId(),account.getName());
           		
						//Logger.log("Storge time: %s, index: %s",Valid_storage_Time[index],index);
      
    
			//======================================================================================================
 			// OPTIONS : SOME INITIAL VARIABLE DECLARATION
  			//---------------------------------------------------------------------------------------------
  						Logger.log('STEP 2: Completed');
  						var AddAdGroupNegative = true;  // Work at AdGroupLevel
 						var AddCampaignNegative = false;
  
  			// These are the selection conditions for our report. Specific AdGroup & specific Campaign
  						var campaignNameContains = "search | alpha ";                      
  						//var adGroupNameContains = "cr�dito pessoal";                                        // MUST CHANGE . TESTING PURPOSE
  
  			// Declare these 3 objects
  						var campaigns ={};
  						var adGroups ={};
  						var exactKeywords =[];
  
			//=======================================================================================================
  			// DECLARE TIME FOR THE REPORT
  			//-----------------------------------------------------------------------------------------------
  						Logger.log('STEP 3: Completed');
                      
 	 					var Time_Threshold;
                      
                      	
  
              			if (Valid_storage_Time[index] == 7)
                    		{Time_Threshold = 'LAST_7_DAYS'}
              			else if (Valid_storage_Time[index] == 14)
                    		{Time_Threshold = 'LAST_14_DAYS'}
              			else if (Valid_storage_Time[index] == 30)
                      		{Time_Threshold = 'LAST_30_DAYS'}
                  		else if (Valid_storage_Time[index] == 1)
                      		{Time_Threshold = 'YESTERDAY'}
                  		else if (Valid_storage_Time[index] == 0)
                      		{Time_Threshold = 'TODAY'}
                      	else if (Valid_storage_Time[index]/10000000 > 1 )
                        	{
                              	var today = new Date();
								var dd = today.getDate();
								var mm = today.getMonth()+1; //January is 0!
								var yyyy = today.getFullYear();

								if	(dd<10)	{ dd = '0'+dd } 
								if	(mm<10) { mm = '0'+mm } 
								today = yyyy+mm+dd;
                                var Starting_Date = Valid_storage_Time[index].toString();
                              	var CUSTOM_DATE = Starting_Date + ',' + today; 
                              
                              	Time_Threshold = CUSTOM_DATE}
                      
 						Logger.log("Time_Threshold: %s", Time_Threshold);
                      
			//=======================================================================================================
  	  		// EXTRACT A LIST OF ALL 'exact match keywords' IN THE ACCOUNT
  			//-----------------------------------------------------------------------------------------------
  						Logger.log('STEP 4: Completed');
  						var Impressions_Limit =0;
  						var columns = ['AdGroupId','Id'];
  						var reportName = 'KEYWORDS_PERFORMANCE_REPORT';
  						if (Valid_storage_Time[index] == "ALL")
                      			{	
                                  	var reportQueryTemplate = 'SELECT %s FROM %s WHERE %s';	                                
                                }
                      	else 	{	
                          			var reportQueryTemplate = 'SELECT %s FROM %s WHERE %s DURING %s';
                        		}
  						var crit=[];
  						crit.push("Impressions > '" +Impressions_Limit+ "'");			  
  						crit.push("KeywordMatchType = EXACT ");                           
        				
                      	Logger.log('STEP 5: Completed');
                      
  			// Generate the report
						if (Valid_storage_Time[index] == "ALL")
                      			{
                                  	var reportQuery = Utilities.formatString(reportQueryTemplate, 
                                           						 			columns.join(','), 
                                           						 			reportName,
                                           						 			crit.join(' AND '));            
                                }
                      	else 	{
									var reportQuery = Utilities.formatString(reportQueryTemplate, 
                                           						 			columns.join(','), 
                                           						 			reportName,
                                           						 			crit.join(' AND '),            
                                           						 			Time_Threshold);
                        		}     
						var report = AdWordsApp.report(reportQuery);

  						Logger.log('STEP 6: Completed');
                      
  			//------------------------------------------------------------------------------------------------
  			// Take the rows from the report. Iterate through them
                      
  						var rows = report.rows();
  						while (rows.hasNext())
   								{
          							var row = rows.next();
          							var keywordId = row['Id'];
          							var adGroupId = row['AdGroupId'];
          							exactKeywords.push(adGroupId + "#" + keywordId);
   								}
                      
  						Logger.log('STEP 7: Completed');
                      
			//=======================================================================================================
  			// EXTRACT A LIST OF ALL 'exact (close variant)' SEARCH QUERIES
 			//-----------------------------------------------------------------------------------------------
  
  						var columns = ['Query','AdGroupId','CampaignId','KeywordId','KeywordTextMatchingQuery','Impressions','QueryMatchTypeWithVariant'];
  						var reportName = 'SEARCH_QUERY_PERFORMANCE_REPORT';
  						var reportQueryTemplate = 'SELECT %s FROM %s WHERE %s DURING %s';
  						var crit=[];
  						crit.push("CampaignName CONTAINS_IGNORE_CASE '" +campaignNameContains+ "' ");	      
  						//crit.push("AdGroupName ='" +adGroupNameContains+ "' ");                               // Must CHANGE. TESTING PURPOSE
        				
                      	Logger.log('STEP 8: Completed');
                      
  			// Generate the report
						var reportQuery = Utilities.formatString(reportQueryTemplate, 
                                                 				 columns.join(','), 
                                                 				 reportName,
                                                                 crit.join(' AND '),
                                                 				 Time_Threshold);      
						var report = AdWordsApp.report(reportQuery);
  						
                        Logger.log('STEP 9: Completed');
                  
  			//------------------------------------------------------------------------------------------------
  	 		// Take the rows from the report. Iterate through them
  
  						var rows = report.rows();
  						while (rows.hasNext())
  								{
          							//Logger.log ('NEXT');
                                  
          							var row = rows.next();
         							var adGroupId = parseInt(row['AdGroupId']);
          							var campaignId = parseInt(row['CampaignId']);
          							var keywordId = parseInt(row['KeywordId']);
          							var searchQuery= row['Query'];
          							var keyword =row['KeywordTextMatchingQuery'];
          							var matchType = row['QueryMatchTypeWithVariant'].toLowerCase();
          
          							if (keyword !== searchQuery && matchType.indexOf("exact (close variant)") !== -1)
          									{	
                  								
                                              	//Logger.log('OK');
                                              
												if (!campaigns.hasOwnProperty(campaignId))
            											{
                          									campaigns[campaignId]=[[],[]];//Logger.log('STEP 1');
                        								}		
          										campaigns[campaignId][0].push(searchQuery);
         		 								campaigns[campaignId][1].push(adGroupId + "#" + keywordId);
          
          										if (!adGroups.hasOwnProperty(adGroupId))
                    									{
                          									adGroups[adGroupId]=[[],[]];//Logger.log('STEP 2');
                    									}
           										adGroups[adGroupId][0].push(searchQuery);
                  								adGroups[adGroupId][1].push(adGroupId + "#" + keywordId);
         
  		       								}
        						}
                       
      					Logger.log('STEP 10: Completed');
                      
			//===========================================================================================================
 			// PARSE DATA CORRECTLY
			//--------------------------------------------------------------------------------------------------
  						var adGroupIds =[];
  						var campaignIds = [];
  						var adGroupNegatives = [];
  						var campaignNegatives = [];
  
  			// 1. -----------------------------------------------------------------------------------------------
  						for (var x in campaigns)
  								{
    								campaignIds.push(parseInt(x));
    								campaignNegatives.push([]);
          
   									for (var y=0;y<campaigns[x][0].length;y++)
    										{
      											var keywordId = campaigns[x][1][y];
          										var keywordText = campaigns[x][0][y];
                      
          										if (exactKeywords.indexOf(keywordId) !== -1 )
            											{
              												campaignNegatives[campaignIds.indexOf(parseInt(x))].push(keywordText);
            											}
    										}
  								}
                      
  						Logger.log('STEP 11: Completed');
                      
  			// 2. ------------------------------------------------------------------------------------------------
  						for (var x in adGroups)
  								{
    								adGroupIds.push(parseInt(x));
    								adGroupNegatives.push([]);
    
          							for (var y=0;y<adGroups[x][0].length;y++)
    										{
         										var keywordId = adGroups[x][1][y];
          										var keywordText = adGroups[x][0][y];
                      
          										if (exactKeywords.indexOf(keywordId) !== -1 )
          												{
             												adGroupNegatives[adGroupIds.indexOf(parseInt(x))].push(keywordText);
          												}
    										}   
  								}
                      
  						Logger.log('STEP 12: Completed');
                
			//===========================================================================================================
  			// CREATE THE NEW NEGATIVE KEYWORDS
  			// --------------------------------------------------------------------------------------------------
  
  						var campaignResults = {};  // Array with the neagative keywords and the destination Campaign
  						var adGroupResults = {};   // Array with the neagative keywords and the destination AdGroup
  
  			// 1. Campaign level --------------------------------------------------------------------------------
 
  						if (AddCampaignNegative)
  							{
    							var campaignIterator = AdWordsApp.campaigns()
    										 					 .withIds(campaignIds)
    										 					 .get();
      
      							while(campaignIterator.hasNext())
        								{
                                          	//Logger.log('new  campaign');
                                          
      										var campaign = campaignIterator.next();
      										var campaignId = campaign.getId();
      										var campaignName = campaign.getName();
      										var campaignIndex = campaignIds.indexOf(campaignId);
      			
              								for (var i = 0; i < campaignNegatives[campaignIndex].length; i++)
                								{
        											campaign.createNegativeKeyword("[" + campaignNegatives[campaignIndex][i] + "]");
                                                    //Logger.log('successfull');
                      
        											if (!campaignResults.hasOwnProperty(campaignName))
                        								{
          													campaignResults[campaignName] = [];
                                                          	//Logger.log('pass');
        												}
        											campaignResults[campaignName].push(campaignNegatives[campaignIndex][i]);
      											}
   	 									}
                              
  								Logger.log('STEP 12.1: Completed');
       				 		}
  
                      
			// 2. AdGroup level -----------------------------------------------------------------------------------
  
  						if (AddAdGroupNegative)
  							{
   								var adGroupIterator = AdWordsApp.adGroups()
    															.withIds(adGroupIds)
    															.get();
      
    							while(adGroupIterator.hasNext())
        								{
                      						//Logger.log('new  adgroup');
                                          
      										var adGroup = adGroupIterator.next();
      										var adGroupId = adGroup.getId();
      										var adGroupName = adGroup.getName(); 	
      										var adGroupIndex = adGroupIds.indexOf(adGroupId);
              
      										for (var i = 0; i < adGroupNegatives[adGroupIndex].length; i++)
                								{
        											adGroup.createNegativeKeyword("[" + adGroupNegatives[adGroupIndex][i] + "]");
                              						//Logger.log('successfull');
                              
        											if (!adGroupResults.hasOwnProperty(adGroupName))
                        								{
          													adGroupResults[adGroupName] = [];
                                      						//Logger.log('pass');
        												}
        											adGroupResults[adGroupName].push(adGroupNegatives[adGroupIndex][i]);
      											}
    									}
                              
          						Logger.log('STEP 12.2: Completed');
  							}
      
			//===============================================================================================================
  			// FORMAT THE RESULTS
  			//-------------------------------------------------------------------------------------------------------
  
  							var resultsString = "The following negative keywords have been added to the following campaigns:";
                      
  							for (var x in campaignResults)
  									{
    									resultsString += "\n\n" + x + ":\n" + campaignResults[x].join("\n");
 									}
                      
  							Logger.log('STEP 13: Completed');
		
  							resultsString += "\n\n\n\nThe following negative keywords have been added to the following AdGroups:";
  							for (var x in adGroupResults)
  									{
    									resultsString += "\n\n" + x + ":\n" + adGroupResults[x].join("\n");
  									}

  							Logger.log(resultsString);
                      
  							Logger.log("END!"); Logger.log("\n");
      		//================================================================================================================
					
                    //}
              
			}
  
 
	//==============================================================================================================================================================
            // Here is the EMAIL part
  
  	SpreadsheetApp.setActiveSpreadsheet(ss_IMPORT);
    var SHEET_NAME_IMPORT_3 = 'Cover Page';
	var sheet_IMPORT_3 = ss_IMPORT.getSheetByName(SHEET_NAME_IMPORT_3);   
    ss_IMPORT.setActiveSheet(sheet_IMPORT_3);
	var sheet_3 = SpreadsheetApp.getActiveSheet(); 
  	var default_email = sheet_3.getRange(5,5,1,1).getValue().toString();
  
  	Logger.log('Email sent to: %s ', default_email);
  	var Notify_Me = default_email;
  	MailApp.sendEmail(Notify_Me,'Exact Match Script [Alpha Campaigns Only] (MCC) - made by Andrei', 
                    "  This is an automatic email generated by the Exact Match Script - [Alpha Campaigns Only] (MCC) script."+
                    "  The script ran succesfully!"+"\n\n"+'   Thank you! '); 
  
  //==============================================================================================================================================================

}


